import { title } from '@/mixins';
import { eventBus, EVENTS } from '@/eventBus';

export default {
	mixins: [title],
	computed: {
		keyTitle() {
			const nameComponent = this.$options.name.toLowerCase();

			return `dashboard.${nameComponent}.title`;
		}
	},
	methods: {
		setTitle() {
			this.title(this.$t(this.keyTitle));
		}
	},
	beforeMount() {
		eventBus.$on(EVENTS.SelectorLanguage.changeLanguage, this.setTitle);

		this.setTitle();
	},
	beforeDestroy() {
		eventBus.$off();
	}
};
