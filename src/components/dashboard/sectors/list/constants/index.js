export const FIELDS = [
	{
		key: 'name',
		sortable: true
	},
	{
		key: 'created_at',
		label: 'Date',
		sortable: true
	},
	'view',
	'actions'
];
