export const SECTIONS = [
	{
		key: 'home',
		needPermission: false,
		icon: 'home',
		linkPath: 'home',
		submenu: false
	},
	{
		key: 'audits',
		needPermission: true,
		icon: 'clipboard',
		linkPath: 'audits.home',
		submenu: false
	},
	{
		key: 'companies',
		name: 'company',
		needPermission: true,
		icon: 'finger-print',
		linkPath: 'company',
		submenu: false
	},
	{
		key: 'equipments',
		needPermission: true,
		icon: 'apps',
		linkPath: 'equipments.home',
		submenu: true
	},
	{
		key: 'sectors',
		needPermission: true,
		icon: 'business',
		linkPath: 'sectors.home',
		submenu: false
	},
	{
		key: 'users',
		needPermission: true,
		icon: 'people',
		linkPath: 'users.home',
		submenu: true
	},
	{
		key: 'account',
		needPermission: false,
		icon: 'settings',
		linkPath: 'account',
		submenu: false
	}
];

export const SUB_SECTIONS = {
	equipments: [
		{
			key: 'list',
			linkPath: 'home',
			permission: 'equipments.read'
		},
		{
			key: 'assignments',
			linkPath: 'assignments.home',
			permission: 'equipments.read'
		},
		{
			key: 'massiveUploads',
			linkPath: 'massiveUploads',
			permission: 'equipments.massive_uploads.read'
		},
		{
			key: 'types',
			linkPath: 'types.home',
			permission: 'equipments.read'
		},
		{
			key: 'massiveUploads',
			linkPath: 'types.massiveUploads',
			permission: 'equipments.massive_uploads.read'
		}
	],
	users: [
		{
			key: 'list',
			linkPath: 'home',
			permission: 'users.read'
		},
		{
			key: 'massiveUploads',
			linkPath: 'massiveUploads',
			permission: 'users.massive_uploads.read'
		}
	]
};
