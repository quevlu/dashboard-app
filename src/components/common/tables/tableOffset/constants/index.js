const offset0 = 15;
const offset1 = 30;
const offset2 = 50;

export default {
	default: offset0,
	records: [offset0, offset1, offset2]
};
