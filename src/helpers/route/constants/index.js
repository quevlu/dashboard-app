const landingDomain = process.env.VUE_APP_LANDING_URL;

export default {
	landing: {
		home: landingDomain,
		plans: `${landingDomain}#plans`
	},
	auth: {
		login: '/auth/login',
		register: '/auth/register',
		sendVerification: '/auth/verifications',
		verifyAccount: '/auth/verifications/${token}',
		sendResetPassword: '/auth/reset-password'
	},
	dashboard: {
		home: '/',
		audits: {
			home: '/audits'
		},
		company: '/companies',
		equipments: {
			home: '/equipments',
			assignments: {
				home: '/equipments/assignments'
			},
			massiveUploads: '/equipments/massive-uploads',
			types: {
				home: '/equipments/types',
				massiveUploads: '/equipments/types/massive-uploads'
			}
		},
		sectors: {
			home: '/sectors',
			create: '/sectors/create',
			view: '/sectors/${id}/view',
			edit: '/sectors/${id}/edit'
		},
		users: {
			home: '/users',
			massiveUploads: '/users/massive-uploads'
		},
		account: '/account'
	}
};
