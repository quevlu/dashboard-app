import routes from './constants';

const route = (name, values = {}) => {
	const partsRoute = name.split('.');
	let routeUrl = routes;

	partsRoute.forEach((index) => {
		routeUrl = routeUrl[index];
	});

	Object.entries(values).forEach((item) => {
		let key = item[0];
		let value = item[1];

		window[key] = value;
	});

	routeUrl = eval(`\`${routeUrl}\``);

	return routeUrl;
};

export default route;
