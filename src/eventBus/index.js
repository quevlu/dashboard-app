import Vue from 'vue';
import constants from './constants';

export const eventBus = new Vue();

export const EVENTS = constants;
