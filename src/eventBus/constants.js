export default {
	SelectorLanguage: {
		changeLanguage: 'changeLanguage'
	},
	TablePagination: {
		handlePage: 'handlePage'
	},
	TableOffset: {
		handleOffset: 'handleOffset'
	}
};
