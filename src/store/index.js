/** @format */
import Vue from 'vue';
import Vuex from 'vuex';

import authModule from './modules/authModule';
import companyModule from './modules/companyModule';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		authModule,
		companyModule
	}
});
