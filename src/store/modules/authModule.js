const ls = require('local-storage');
import { authService } from '@/http/services';
import router from '@/router';
import { route } from '@/helpers';
import { JWT } from '@/constants';

export default {
	namespaced: true,
	state: () => ({
		user: null
	}),
	actions: {
		login({ commit }, response) {
			ls.set(JWT.token, response.token);

			commit('userLogged');
			commit('companyModule/getCompany', null, { root: true });

			router.push(route('dashboard.home')).catch(() => {});
		},
		userLogged: ({ commit }) => commit('userLogged'),
		logout: ({ commit }) => commit('logout'),
		refresh: ({ commit }, token) => commit('refresh', token)
	},
	mutations: {
		userLogged(state) {
			if (ls.get(JWT.token)) {
				authService.userLogged().then((response) => {
					state.user = response;

					if (router.currentRoute.fullPath.startsWith('/auth')) {
						router.push(route('dashboard.home')).catch(() => {});
					}
				});
			}
		},
		logout(state, invalidateJwt = false) {
			state.user = null;

			if (invalidateJwt) {
				authService.logout().finally(() => ls.remove(JWT.token));
			} else {
				ls.remove(JWT.token);
			}

			router.push(route('auth.login')).catch(() => {});
		},
		refresh: (state, token) => ls.set(JWT.token, token)
	},
	getters: {
		getUser: (state) => state.user,
		getPermissions: (state) => (state.user ? state.user.permissions : [])
	}
};
