const ls = require('local-storage');
import { companyService } from '@/http/services';
import { JWT } from '@/constants';

export default {
	namespaced: true,
	state: () => ({
		company: null
	}),
	actions: {
		getCompany: ({ commit }) => commit('getCompany')
	},
	mutations: {
		getCompany(state) {
			if (ls.get(JWT.token)) {
				companyService.get().then((response) => (state.company = response));
			}
		}
	},
	getters: {
		getCompany: (state) => state.company || {}
	}
};
