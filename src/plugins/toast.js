import Vue from 'vue';
import Toast from 'vue-toastification';

Vue.use(Toast, {
	transition: 'Vue-Toastification__fade',
	maxToasts: 5,
	newestOnTop: true,
	filterBeforeCreate: (toast, toasts) => {
		if (toasts.filter((t) => t.type === toast.type).length !== 0) {
			return false;
		}
		return toast;
	}
});

const defaultOptions = {
	position: 'bottom-right',
	timeout: 3000,
	closeOnClick: true,
	pauseOnFocusLoss: false,
	pauseOnHover: true,
	draggable: false,
	draggablePercent: 0.3,
	showCloseButtonOnHover: false,
	hideProgressBar: true,
	closeButton: false,
	icon: false,
	rtl: false
};

export default {
	success: (message) => Vue.$toast.success(message, defaultOptions),
	error: (message) => Vue.$toast.error(message, defaultOptions),
	info: (message) => Vue.$toast.info(message, defaultOptions),
	warning: (message) => Vue.$toast.warning(message, defaultOptions)
};
