import Vue from 'vue';

Vue.config.productionTip = process.env.VUE_APP_PRODUCTION_TIP;
Vue.config.devtools = process.env.VUE_APP_DEV_TOOLS;
Vue.config.performance = process.env.VUE_APP_PERFORMANCE;
Vue.config.silent = process.env.VUE_APP_SILENT;
