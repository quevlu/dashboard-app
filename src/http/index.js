import axios from 'axios';
import { HTTP_CODES, NETWORK_EXCEPTION } from './constants';
import { toast } from '@/plugins';
import { i18n } from '@/i18n';
import store from '@/store';
import { JWT } from '@/constants';
const fileDownload = require('js-file-download');

const ls = require('local-storage');
const queryString = require('query-string');
const isEmptyObj = require('is-empty-obj');

const API = process.env.VUE_APP_DASHBOARD_API_URL;
const TIMEOUT = 6000;
const REFRESH_ENDPOINT = 'auth';

const http = axios.create({
	baseURL: API,
	timeout: TIMEOUT
});

let isRefreshing = false;
let subscribers = [];

const onRefreshed = ({ access_token }) => subscribers.map((cb) => cb(access_token));

const subscribeTokenRefresh = (cb) => subscribers.push(cb);

http.interceptors.request.use(
	(config) => {
		const token = ls.get(JWT.token);

		if (config.url.includes('csv=true')) {
			config.responseType = 'blob';
		}

		if (token) {
			config.headers.Authorization = `${JWT.prefix} ${token}`;
		}

		return config;
	},
	(err) => {
		return Promise.reject(err);
	}
);

http.interceptors.response.use(
	(response) => response,
	(err) => {
		const { config } = err;
		const originalRequest = config;

		if (!err.response) {
			throw NETWORK_EXCEPTION;
		}

		if (err.response.status == HTTP_CODES.UNAUTHORIZED) {
			if (!isRefreshing) {
				isRefreshing = true;
				const token = ls.get(JWT.token);

				http
					.put(REFRESH_ENDPOINT, {
						headers: {
							authorization: `${JWT.prefix} ${token}`
						}
					})
					.then((response) => {
						const refreshToken = response.token;

						store.commit('authModule/refresh', refreshToken);

						isRefreshing = false;
						onRefreshed(refreshToken);
						subscribers = [];
					})
					.catch((err) => {
						if (err && err.response.status != HTTP_CODES.UNAUTHORIZED) {
							store.commit('authModule/logout');
						}
					});
			} else if (config.url === REFRESH_ENDPOINT) {
				store.commit('authModule/logout');
			}

			return new Promise((resolve) => {
				subscribeTokenRefresh((token) => {
					originalRequest.headers.Authorization = `${JWT.prefix} ${token}`;
					resolve(http(originalRequest));
				});
			});
		}

		return Promise.reject(err);
	}
);

http.interceptors.response.use(
	(response) => {
		const data = response.data ? response.data.data : response;

		if (response.config.url.includes('csv=true')) {
			const fileName = response.headers['content-disposition'].match(/[a-z-0-9]{1,}\.csv/)[0];

			fileDownload(response.data, fileName);
		}

		return Promise.resolve(data);
	},
	(error) => {
		let message = null;
		if (error == NETWORK_EXCEPTION) {
			message = i18n.t('customErrors.network');
			store.commit('authModule/logout');
		}

		if (error.response && error.response.status == HTTP_CODES.INTERNAL_SERVER_ERROR) {
			message = i18n.t('customErrors.internalServerError');
		}

		if (error.response && error.response.status == HTTP_CODES.TOO_MANY_REQUESTS) {
			message = error.response.data.error.code;
		}

		if (message) {
			toast.error(message);
		}

		return Promise.reject(error);
	}
);

http.createQueryString = (table = {}, total = false, csv = false) => {
	const params = {
		page: table.page,
		offset: table.offset
	};

	if (total) {
		params.total = true;
	}

	if (csv) {
		delete params.total;
		delete params.page;
		delete params.offset;

		params.csv = true;
	}

	if (!isEmptyObj(table.order_by)) {
		for (const orderBy in table.order_by) {
			const key = `order_by[${orderBy}]`;
			params[key] = table.order_by[orderBy];
		}
	}

	if (!isEmptyObj(table.filters)) {
		for (const filter in table.filters) {
			params[filter] = table.filters[filter];
		}
	}

	return `?${queryString.stringify(params)}`;
};

export default http;
