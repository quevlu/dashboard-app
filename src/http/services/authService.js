import http from '@/http';

const prefixRoute = '/auth';

export default {
	login: (user) => http.post(prefixRoute, user),
	userLogged: () => http.get(prefixRoute),
	register: (user) => http.post(`${prefixRoute}/register`, user),
	verification: (user) => http.post(`${prefixRoute}/verifications`, user),
	verify: (user) =>
		http.delete(`${prefixRoute}/verifications/${user.token}`, {
			data: {
				captcha: user.captcha
			}
		}),
	sendResetPassword: (user) => http.post(`${prefixRoute}/reset-passwords`, user),
	resetPassword: (user) => http.put(`${prefixRoute}/reset-passwords/${user.token}`, user),
	changePassword: (user) => http.put(`${prefixRoute}/change-password`, user)
};
