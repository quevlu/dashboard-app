import http from '@/http';

export default {
	all: () => http.get('/plans')
};
