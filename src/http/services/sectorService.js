import http from '@/http';

const prefixRoute = '/sectors';

export default {
	all: (table, total = false, csv = false) => {
		const queryParam = http.createQueryString(table, total, csv);

		return http.get(`${prefixRoute}${queryParam}`);
	},
	delete: (id) => http.delete(`${prefixRoute}/${id}`)
};
