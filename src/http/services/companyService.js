import http from '@/http';

const route = '/companies/current';

export default {
	get: () => http.get(route),
	update: (company) => {
		let formData = new FormData();

		for (const property in company) {
			formData.append(property, company[property]);
		}

		return http.post(route, formData, {
			headers: {
				'Content-Type': 'multipart/form-data'
			}
		});
	},
	delete: (company) =>
		http.delete(route, {
			data: company
		})
};
