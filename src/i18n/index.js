import Vue from 'vue';
import VueI18n from 'vue-i18n';
import http from '@/http';

const ls = require('local-storage');

Vue.use(VueI18n);

const setHttpLanguage = (language) => (http.defaults.headers.common['Accept-Language'] = language);

const setI18nLanguage = (language) => {
	i18n.locale = language;
	document.querySelector('html').setAttribute('lang', language);

	setHttpLanguage(language);

	ls.set(langKey, language);

	return language;
};

const getLanguage = () => {
	const storedLang = ls.get(langKey);
	const language = storedLang || window.navigator.userLanguage || window.navigator.language;

	return defaultLanguages.includes(language) ? language : defaultLanguage;
};

const setLanguage = () => loadLanguageAsync(getLanguage());

const loadLanguageAsync = (language) => {
	if (i18n.locale !== language) {
		return import(`./locales/${language}`).then((msgs) => {
			i18n.setLocaleMessage(language, msgs.default);

			return setI18nLanguage(language);
		});
	}
	return Promise.resolve(language);
};

const langKey = 'lang';
const defaultLanguages = ['en', 'es'];
const defaultLanguage = 'es';
const actualLanguage = getLanguage();

const messages = require(`./locales/${actualLanguage}.json`);

const i18n = new VueI18n({
	locale: actualLanguage,
	fallbackLocale: defaultLanguage,
	silentTranslationWarn: true,
	messages: {
		[actualLanguage]: messages
	}
});

setHttpLanguage(actualLanguage);

export { getLanguage, setLanguage, loadLanguageAsync, i18n };
