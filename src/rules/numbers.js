export default (value) => {
    var re = /[0-9]/;

    return re.test(value);
};