/** @format */

export default (value) => {
  var re = /[A-ZÁÉÍÓÚÑ]/;

  return re.test(value);
};
