/** @format */

export default (value) => {
  var re = /[a-zA-ZáéíóúÁÉÍÓÚÑñ]/g;

  return re.test(value);
};
