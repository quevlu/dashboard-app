export default (param) => (value, vm) => value != vm[param];
