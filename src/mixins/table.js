import { eventBus, EVENTS } from '@/eventBus';

const queryString = require('query-string');

export default {
	data: () => ({
		table: {
			total: null,
			records: [],
			filters: {},
			order_by: {},
			page: 1,
			offset: 15,
			isLoading: false
		}
	}),
	watch: {
		table: {
			handler() {
				this.pushQueryParams();
			},
			deep: true
		}
	},
	methods: {
		setPage(page) {
			this.table.page = page;

			this.get();
		},
		setOffset(offset) {
			this.table.offset = offset;

			this.get();
		},
		sorting(thRow) {
			const { sortBy, sortDesc } = thRow;

			if (sortBy) {
				this.table.order_by[sortBy] = sortDesc ? 'desc' : 'asc';

				this.get();
			}
		},
		pushQueryParams() {
			const url = new URL(window.location);
			url.searchParams.set('page', this.table.page);
			url.searchParams.set('offset', this.table.offset);

			for (const filter in this.table.filters) {
				url.searchParams.set(filter, this.table.filters[filter]);
			}

			for (const orderBy in this.table.order_by) {
				url.searchParams.set(`order_by[${orderBy}]`, this.table.order_by[orderBy]);
			}

			window.history.pushState({}, '', url);
		},
		setFromQueryString() {
			const parsed = queryString.parse(location.search);

			if (parsed.offset) {
				this.table.offset = parseInt(parsed.offset);
				delete parsed.offset;
			}

			if (parsed.page) {
				this.table.page = parseInt(parsed.page);
				delete parsed.page;
			}

			for (const key in parsed) {
				key.replace(/\[[a-z_]{1,}\]/g, (match) => {
					const column = match.replace('[', '').replace(']', '');
					const keyQueryParam = `order_by${match}`;

					this.table.order_by[column] = parsed[keyQueryParam];

					delete parsed[keyQueryParam];
				});
			}

			for (const key in parsed) {
				this.table.filters[key] = parsed[key];
				delete parsed[key];
			}
		},
		get() {
			this.table.isLoading = true;

			this.getRecords(true).then((response) => (this.table.total = response.total));

			this.getRecords()
				.then((response) => (this.table.records = response))
				.finally(() => (this.table.isLoading = false));
		},
		exportCsv() {
			this.getRecords(false, true);
		}
	},
	beforeMount() {
		eventBus.$on(EVENTS.TablePagination.handlePage, this.setPage);
		eventBus.$on(EVENTS.TableOffset.handleOffset, this.setOffset);

		this.setFromQueryString();
		this.get();
	},
	beforeDestroy() {
		eventBus.$off();
	}
};
