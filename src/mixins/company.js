import { mapGetters } from 'vuex';

export default {
	computed: {
		...mapGetters({
			company: 'companyModule/getCompany'
		}),
		logo() {
			return this.company.logo || require('@/assets/logo.png');
		}
	}
};
