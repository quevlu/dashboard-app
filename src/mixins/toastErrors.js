import { toast } from '@/plugins';
import { HTTP_CODES } from '@/http/constants';

export default {
	methods: {
		errorsToToast(errorResponse) {
			if (errorResponse.response && errorResponse.response.status == HTTP_CODES.INVALID_REQUEST) {
				const errors = errorResponse.response.data.errors;
				let message = '';

				for (const error in errors) {
					message += `${errors[error]} \n`;
				}

				toast.error(message);
			}
		}
	}
};
