import { HTTP_CODES } from '@/http/constants';

export default {
	data: () => ({
		isLoading: false,
		disableAction: true,
		submitted: false
	}),
	methods: {
		revertSubmit() {
			if (this.disableAction) this.submitted = false;
		},
		loading() {
			this.isLoading = true;
		},
		revertLoading() {
			this.isLoading = false;
		},
		assignErrors(error) {
			const { response } = error;

			if (response && response.status == HTTP_CODES.INVALID_REQUEST) {
				const { data } = response;

				this.errors = data.errors;
			}
		},
		cleanForm(modelProperty = 'form') {
			this[modelProperty] = {};
		}
	}
};
