export default {
	methods: {
		title: (title) => (document.title = `${title} | Binaccle`)
	}
};
