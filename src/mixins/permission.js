import { mapGetters, mapActions } from 'vuex';

export default {
	computed: mapGetters({
		permissions: 'authModule/getPermissions'
	}),
	methods: {
		...mapActions({
			hasPermission: 'authModule/hasPermission'
		}),
		hasModulePermission(module) {
			return this.permissions.some((userPermission) => userPermission.startsWith(module));
		},
		hasPermission(permission) {
			return this.permissions.some((userPermission) => userPermission == permission);
		}
	}
};
