import Vue from 'vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';

Vue.use(VueReCaptcha, {
	siteKey: process.env.VUE_APP_RE_CAPTCHA_V3_KEY,
	loaderOptions: {
		useRecaptchaNet: true,
		autoHideBadge: true
	}
});

export default {
	methods: {
		async reCaptchaToken() {
			await this.$recaptchaLoaded();

			return await this.$recaptcha(this.$options.name);
		},
		async reCaptchaTokenAndMerge(dataProperty = 'form') {
			const token = await this.reCaptchaToken();

			this[dataProperty].captcha = token;
		}
	}
};
