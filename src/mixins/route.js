import { route as routeHelper } from '@/helpers';

export default {
	methods: {
		route: routeHelper
	}
};
