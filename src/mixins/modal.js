export default {
	props: {
		value: Boolean
	},
	data() {
		return {
			showModal: this.value
		};
	},
	watch: {
		value(value) {
			this.showModal = value;
		},
		showModal(value) {
			if (!value) this.close();
		}
	},
	methods: {
		close() {
			this.$emit('input', false);
		}
	}
};
