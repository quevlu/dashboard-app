export default {
	data: () => ({
		errors: {}
	}),
	methods: {
		validate() {
			this.submitted = true;
			this.$v.$touch();

			return this.$v.$invalid;
		},
		extractErrors(modelEntity) {
			if (this.submitted && !this.disableAction) {
				let errorMessage = null;

				const splitModelEntity = modelEntity.split('.');
				const errorModelObject = splitModelEntity.reduce((a, b) => a[b], this.$v);
				const attribute = splitModelEntity.pop();

				if (errorModelObject && errorModelObject.$anyError) {
					delete this.errors[attribute];

					const rules = Object.entries(errorModelObject.$params);

					for (const [rule] of rules) {
						if (!errorModelObject[rule]) {
							errorMessage = this.$t(`errors.${rule}`);
							const keyAttribute = `attributes.${attribute}`;
							const attributeTranslated = this.$t(keyAttribute);

							const params = errorModelObject.$params[rule] || {};
							params.attribute =
								attributeTranslated != keyAttribute ? attributeTranslated : attribute.replace('_', ' ');

							errorMessage = errorMessage.replace(/(:[a-zA-Z]{1,})/g, (match) => params[match.replace(':', '')]);

							break;
						}
					}

					return errorMessage;
				} else {
					const count = (modelEntity.match(/\./g) || []).length;
					const key = count == 1 ? attribute : modelEntity;

					errorMessage = this.errors[key];
				}

				return errorMessage;
			}
		}
	}
};
