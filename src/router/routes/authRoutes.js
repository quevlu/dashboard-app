import { isLoggedIn } from '../middlewares';
import { route } from '@/helpers';

const components = [
	{
		path: route('auth.login'),
		name: 'Login',
		pathFile: 'Login'
	},
	{
		path: route('auth.register'),
		name: 'Register',
		pathFile: 'register/Register'
	},
	{
		path: route('auth.sendVerification'),
		name: 'SendVerification',
		pathFile: 'verification/SendVerification'
	},
	{
		path: `${route('auth.sendVerification')}/:token`,
		name: 'VerifyAccount',
		pathFile: 'verification/VerifyAccount'
	},
	{
		path: route('auth.sendResetPassword'),
		name: 'SendResetPassword',
		pathFile: 'resetPassword/SendResetPassword'
	},
	{
		path: `${route('auth.sendResetPassword')}/:token`,
		name: 'ResetPassword',
		pathFile: 'resetPassword/ResetPassword'
	}
];

components.forEach((component) => {
	component.component = () => import(`@/components/auth/${component.pathFile}`);
	component.beforeEnter = (to, from, next) => isLoggedIn(to, from, next);
});

export default components;
