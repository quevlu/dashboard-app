import { isLoggedIn } from '../middlewares';
import { route } from '@/helpers';

const sections = [
	{
		keyPath: 'home',
		name: 'Home',
		component: 'home/Home'
	},
	// {
	// 	path: route('dashboard.audits.home'),
	// 	name: `${dashboardPrefix}AuditsHome`
	// },
	{
		keyPath: 'company',
		name: 'Company',
		component: 'company/Company'
	},
	{
		keyPath: 'sectors.home',
		name: 'SectorList',
		component: 'sectors/list/SectorList'
	},
	// {
	// 	path: route('dashboard.equipments.home'),
	// 	name: `${dashboardPrefix}EquipmentsHome`
	// },
	// {
	// 	path: route('dashboard.equipments.assignments.home'),
	// 	name: `${dashboardPrefix}EquipmentsAssignmentsHome`
	// },
	// {
	// 	path: route('dashboard.equipments.massiveUploads'),
	// 	name: `${dashboardPrefix}EquipmentsMassiveUploads`
	// },
	// {
	// 	path: route('dashboard.equipments.types.home'),
	// 	name: `${dashboardPrefix}EquipmentsTypesHome`
	// },
	// {
	// 	path: route('dashboard.equipments.types.massiveUploads'),
	// 	name: `${dashboardPrefix}EquipmentsTypesMassiveUploads`
	// },
	// {
	// 	path: route('dashboard.sectors'),
	// 	name: `${dashboardPrefix}Sectors`
	// },
	// {
	// 	path: route('dashboard.users.home'),
	// 	name: `${dashboardPrefix}UsersHome`
	// },
	// {
	// 	path: route('dashboard.users.massiveUploads'),
	// 	name: `${dashboardPrefix}UsersMassiveUploads`
	// },
	{
		keyPath: 'account',
		name: `Account`,
		component: 'account/Account'
	}
];

const components = sections.map((section) => ({
	path: route(`dashboard.${section.keyPath}`),
	name: `Dashboard${section.name}`,
	component: () => import(`@/components/dashboard/${section.component}`)
}));

components.forEach((component) => {
	component.beforeEnter = (to, from, next) => isLoggedIn(to, from, next);
});

export default components;
