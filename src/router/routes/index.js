import authRoutes from './authRoutes';
import dashboardRoutes from './dashboardRoutes';
import commonRoutes from './commonRoutes';

export default [...authRoutes, ...dashboardRoutes, ...commonRoutes];
