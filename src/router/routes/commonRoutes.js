export default [
	{
		path: '*',
		name: 'NotFound',
		pathFile: 'Login',
		component: () => import('@/components/notFound/NotFound')
	}
];
