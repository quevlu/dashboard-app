import { route } from '@/helpers';
import { JWT } from '@/constants';
const ls = require('local-storage');

export default (to, from, next) => {
	if (ls.get(JWT.token) && to.path.startsWith('/auth')) {
		next(route('dashboard.home'));
	} else {
		next();
	}
};
