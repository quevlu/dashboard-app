import Vue from 'vue';
import App from './App.vue';

import { i18n } from './i18n';
import router from '@/router';
import store from './store';

import '@/sass/_app.scss';

import '@/core/config';

new Vue({
	i18n,
	router,
	store,
	render: (h) => h(App)
}).$mount('#app');

window.addEventListener('message', (e) => {
	if (e.data && typeof e.data === 'string' && e.data.match(/webpackHotUpdate/)) {
		console.log('hot reload happened');
		console.clear();
	}
});
