# Conduct Code

Vue components

1. Imports
  
- components
- libraries

2. Definitions

- mixins
- directives
- components
- props
- data
- validations
- watch
- computed
- methods
  - mapActions
  - ...