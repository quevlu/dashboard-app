const { defineConfig } = require('@vue/cli-service');
const path = require('path');

module.exports = defineConfig({
	publicPath: '/',
	devServer: {
		hot: true,
		host: 'dashboard.binaccle.org',
		https: true,
		port: 3003,
		historyApiFallback: true
	},
	css: {
		loaderOptions: {
			sass: {
				additionalData: `@import "@/sass/_variables.scss";`
			}
		}
	}
});
